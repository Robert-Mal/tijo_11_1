package pl.edu.pwsztar.chess.domain;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BishopTest {

    private final RulesOfGame bishop = new RulesOfGame.Bishop();

    @Tag("Bishop")
    @ParameterizedTest
    @CsvSource({
            "0, 0, 7, 7",
            "1, 0, 3, 2",
            "1, 5, 3, 3",
            "4, 7, 1, 4"
    })
    void checkCorrectMoveForBishop(int xStart, int yStart, int xStop, int yStop) {
        assertTrue(bishop.isCorrectMove(new Point(xStart, yStart), new Point(xStop, yStop)));
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 1, 0",
            "3, 4, 5, 5"
    })
    void checkIncorrectMoveForBishop(int xStart, int yStart, int xStop, int yStop) {
        assertFalse(bishop.isCorrectMove(new Point(xStart, yStart), new Point(xStop, yStop)));
    }
}
