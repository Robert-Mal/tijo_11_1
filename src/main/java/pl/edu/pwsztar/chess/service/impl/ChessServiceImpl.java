package pl.edu.pwsztar.chess.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.chess.domain.RulesOfGame;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;
import pl.edu.pwsztar.chess.mapper.PointMapper;
import pl.edu.pwsztar.chess.service.ChessService;

@Service
@Transactional
public class ChessServiceImpl implements ChessService {
    private final RulesOfGame bishop;
    private final RulesOfGame king;
    private final RulesOfGame knight;
    private final RulesOfGame pawn;
    private final RulesOfGame queen;
    private final RulesOfGame rook;
    private final PointMapper pointMapper;

    public ChessServiceImpl() {
        bishop = new RulesOfGame.Bishop();
        king = new RulesOfGame.King();
        knight = new RulesOfGame.Knight();
        pawn = new RulesOfGame.Pawn();
        queen = new RulesOfGame.Queen();
        rook = new RulesOfGame.Rook();
        pointMapper = new PointMapper();
    }

    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {

        switch (figureMoveDto.getType()) {
            case BISHOP:
                return bishop.isCorrectMove(pointMapper.placementToPoint2D(figureMoveDto.getSource()),
                    pointMapper.placementToPoint2D(figureMoveDto.getDestination()));
            case KING:
                return king.isCorrectMove(pointMapper.placementToPoint2D(figureMoveDto.getSource()),
                    pointMapper.placementToPoint2D(figureMoveDto.getDestination()));
            case KNIGHT:
                return knight.isCorrectMove(pointMapper.placementToPoint2D(figureMoveDto.getSource()),
                        pointMapper.placementToPoint2D(figureMoveDto.getDestination()));
            case PAWN:
                return pawn.isCorrectMove(pointMapper.placementToPoint2D(figureMoveDto.getSource()),
                        pointMapper.placementToPoint2D(figureMoveDto.getDestination()));
            case QUEEN:
                return queen.isCorrectMove(pointMapper.placementToPoint2D(figureMoveDto.getSource()),
                    pointMapper.placementToPoint2D(figureMoveDto.getDestination()));
            case ROOK:
                return rook.isCorrectMove(pointMapper.placementToPoint2D(figureMoveDto.getSource()),
                    pointMapper.placementToPoint2D(figureMoveDto.getDestination()));
        }

        return false;
    }
}
