package pl.edu.pwsztar.chess.domain;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * source na destination w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(Point source, Point destination);

    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                return false;
            }

            return Math.abs(destination.getX() - source.getX()) ==
                    Math.abs(destination.getY() - source.getY());
        }
    }

    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            return destination.getX() == source.getX() + 1 || destination.getX() == source.getX() - 1 ||
                destination.getY() == source.getY() + 1 || destination.getY() == source.getY() - 1;
        }
    }

    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            int row = Math.abs(destination.getX() - source.getX());
            int column = Math.abs(destination.getY() - source.getY());
            return ((row == 2 && column == 1) || (row == 1 && column == 2));
        }
    }

    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if (source.getY() == 1) {
                return destination.getX() == source.getX() &&
                        (destination.getY() == source.getY() + 1 || destination.getY() == source.getY() + 2);
            } else {
                return destination.getX() == source.getX() && destination.getY() == source.getY() + 1;
            }
        }
    }

    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            return ((Math.abs(destination.getX() - source.getX()) ==
                Math.abs(destination.getY() - source.getY())) ||
                (source.getX() == destination.getX() || source.getY() == destination.getY()));
        }
    }

    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            return source.getX() == destination.getX() || source.getY() == destination.getY();
        }
    }
}
