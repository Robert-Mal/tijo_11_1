package pl.edu.pwsztar.chess.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.chess.domain.Point;

@Component
public class PointMapper {

  public Point placementToPoint2D(String placement) {

    return new Point(Character.getNumericValue(placement.charAt(0))-10,
        Character.getNumericValue(placement.charAt(2))-1);
  }
}
